import { useEffect, useState } from 'react'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import ToggleButton from 'react-bootstrap/ToggleButton'

const MovieItem = ({ film, onClick, className, filmName }) => {
	const [radioValue, setRadioValue] = useState('')

	useEffect(() => {
		const value = ''
		if (filmName !== film.name) {
			setRadioValue(value)
		}
	}, [film.name, filmName])

	return (
		<ButtonGroup size="sm">
			{film.session.map((radio, idx) => {
				return (
					<ToggleButton
						variant="dark"
						htmlFor={`${film.name}-${idx}`}
						key={`${film.name}-${idx}`}
						id={`${film.name}-${idx}`}
						type="radio"
						className={className}
						onClick={onClick}
						name={`${film.name}-${idx}`}
						value={radio.time}
						checked={radioValue === radio.time}
						onChange={e => setRadioValue(e.currentTarget.value)}
					>
						{radio.time}
					</ToggleButton>
				)
			})}
		</ButtonGroup>
	)
}

export default MovieItem
