import styles from "./card.module.css";

const Card = ({ children }) => {
  return <div className={styles.films}>{children}</div>;
};

export default Card;
