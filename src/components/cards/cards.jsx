import MovieCards from "./movieCards/movieCards";
import StarCards from "./starCards/starCards";

import styles from "./cards.module.css";

const Cards = ({ click }) => {
  return (
    <>
      <div className={styles.cardsWrap}>
        <h2>Trending Movies</h2>
        <div className={styles.boxItem}>
          <MovieCards />
        </div>
      </div>
      <div className={styles.cardsWrap}>
        <h2>Trending Stars</h2>
        <div className={styles.boxItem}>
          <StarCards click={click} />
        </div>
      </div>
    </>
  );
};

export default Cards;
