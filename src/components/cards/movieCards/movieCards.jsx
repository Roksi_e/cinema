import { useContext, useState } from 'react'
import { Link } from 'react-router-dom'
import Card from '../card/card'
import MovieItem from '../movieItem/movieItem'
import Button from '../../button/button'
import { useTicket } from '../../../methods/tickets'
import { MyContext } from '../../../methods/context'

import styles from './movieCards.module.css'

const MovieCards = () => {
	const { films } = useContext(MyContext)
	const { addSession, openHall } = useTicket()
	const [filmName, setFilmName] = useState('')

	const handleAddSession = (e, obj) => {
		addSession(e.target.textContent, obj)
		setFilmName(obj.name)
	}

	return (
		<>
			{films
				? films.map((film, i) => {
						return (
							<Card key={`${film.name}-${film.id}`}>
								<Link to={`films/${film.id}`}>
									<div className={styles.itemImg}>
										<img src={film.image} alt={film.name} />
									</div>
								</Link>
								<div className={styles.itemBottom}>
									<div className={styles.itemTitle}>
										{film.name.length > 15 ? `${film.name.slice(0, 15)}...` : film.name}
									</div>
									<div className={styles.itemDate}>{film.rfilmease_date}</div>
								</div>
								<div className={styles.movieSession}>
									<div className={styles.itemSession}>
										<MovieItem
											filmName={filmName}
											className={styles.items}
											film={film}
											onClick={e => {
												handleAddSession(e, film)
											}}
										/>
									</div>
									<Link to={`films/${film.id}`}>
										<Button
											disabled={filmName !== film.name ? true : false}
											onClick={openHall}
											className={styles.button}
										/>
									</Link>
								</div>
							</Card>
						)
				  })
				: null}
		</>
	)
}

export default MovieCards
