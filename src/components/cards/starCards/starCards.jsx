import { useContext } from 'react'
import { v4 as uuidv4 } from 'uuid'
import Card from '../card/card'
import { MyContext } from '../../../methods/context'

import styles from './starCards.module.css'

const StarCards = ({ click }) => {
	const { stars } = useContext(MyContext)

	return (
		<>
			{stars
				? stars.map(el => {
						return (
							<Card click={click} key={uuidv4()}>
								<div
									onClick={() => {
										click(el.id)
									}}
									className={styles.itemImg}
								>
									<img src={el.image} alt={el.name} />
								</div>
								<div className={styles.itemBottom}>
									<div className={styles.itemTitle}>{el.name}</div>
								</div>
							</Card>
						)
				  })
				: null}
		</>
	)
}

export default StarCards
