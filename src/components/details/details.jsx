import { useContext, useState } from 'react'
import { BsStarFill } from 'react-icons/bs'
import { useParams } from 'react-router-dom'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import ToggleButton from 'react-bootstrap/ToggleButton'
import { MyContext } from '../../methods/context'
import { useTicket } from '../../methods/tickets'
import Button from '../button/button'
import CinemaHall from '../cinemaHall/cinemaHall'
import ClientTickets from '../clientTickets/clientTickets'
import ControlForm from '../controlForm/controlForm'

import styles from './details.module.css'

const Details = () => {
	const { id } = useParams()
	const allData = useContext(MyContext)
	const { addSession, isOpen, isActive, isOpenForm, openHall } = useTicket()
	const [isActiveBtn, setIsActiveBtn] = useState(false)
	const [radioValue, setRadioValue] = useState('')

	const film = allData.films.find(film => {
		return film.id === parseInt(id)
	})

	const handleAddSession = (e, obj) => {
		addSession(e.target.textContent, obj)
		setIsActiveBtn(true)
	}

	const openHallHandler = () => {
		const value = ''
		openHall()
		setIsActiveBtn(false)
		setRadioValue(value)
	}

	return (
		<div className={styles.filmWrap}>
			{isOpen ? <CinemaHall /> : null}
			{isActive ? <ClientTickets /> : null}
			{isOpenForm ? <ControlForm /> : null}
			<div className={styles.filmBackdrop}>
				<img src={film.backdrop} alt={film.name} />
			</div>
			<div className={styles.filmContent}>
				<div className={styles.filmPoster}>
					<img src={film.image} alt={film.name} />
				</div>
				<div className={styles.filmInfo}>
					<h2 className={styles.filmTitle}>{film.name}</h2>
					<div className={styles.filmBottom}>
						<BsStarFill className={styles.icon} />
						<div>{film.rate.toFixed(1)}</div>
					</div>
					<div className={styles.filmBottom}>
						<ButtonGroup>
							{film.session.map((radio, idx) => {
								return (
									<ToggleButton
										key={idx}
										id={`radio-${idx}`}
										type="radio"
										className={styles.items}
										onClick={e => {
											handleAddSession(e, film)
											console.log(e.target)
										}}
										name="radio"
										value={radio.time}
										checked={radioValue === radio.time}
										onChange={e => setRadioValue(e.currentTarget.value)}
									>
										{radio.time}
									</ToggleButton>
								)
							})}
						</ButtonGroup>
						<Button disabled={!isActiveBtn ? true : false} onClick={openHallHandler} className={styles.button} />
					</div>
				</div>
				<div className={styles.filmDescription}>
					Overview
					<p className={styles.filmOverview}>{film.overview}</p>
				</div>
			</div>
		</div>
	)
}

export default Details
