import { useState } from 'react'
import { AiOutlineClose } from 'react-icons/ai'
import { IoCalendarNumberOutline } from 'react-icons/io5'
import screen from '../../img/screen.svg'
import SeatsBottom from './seatsBottom/seatsBottom'
import TicketInfo from './ticketInfo/ticketInfo'
import { useTicket } from '../../methods/tickets'
import CinemaSeats from './cinemaSeats/cinemaSeats'
import { months } from '../../methods'
import CinemaCalendar from '../cinemaCalendar/cinemaCalendar'

import styles from './cinemaHall.module.css'

const CinemaHall = () => {
	const { closeCalendar, openCalendar, closeHall, isOpen, isOpenCalendar } = useTicket()
	const [isOpenForm, setIsOpenForm] = useState(false)

	const toggleOpenCalendar = value => {
		isOpenForm
			? closeCalendar(`${months[new Date().getMonth()]} ${new Date().getDate()} ${new Date().getFullYear()}`)
			: openCalendar()
		setIsOpenForm(prev => !prev)
	}

	return (
		<>
			{isOpen ? (
				<div className={styles.cinemaWrap}>
					<div className={styles.cinemaHall}>
						<AiOutlineClose
							onClick={() => {
								closeHall()
							}}
							className={styles.cinemaClose}
						/>
						<div className={styles.cinemaTitle}> Choose seats</div>
						<IoCalendarNumberOutline className={styles.cinemaDate} onClick={toggleOpenCalendar} />
						{isOpenCalendar ? (
							<div className={styles.calendar}>
								<CinemaCalendar className={styles.btnCalendar} />
							</div>
						) : null}
						<div>
							<img src={screen} alt="screen" />
						</div>
						<div className={styles.seatsAll}>
							<div className={styles.cinemaSeats}>
								<CinemaSeats />
							</div>
							<SeatsBottom />
						</div>
						<TicketInfo />
					</div>
				</div>
			) : null}
		</>
	)
}

export default CinemaHall
