import React, { useState } from 'react'
import { useTicket } from '../../../methods/tickets'
import chairWhite from '../../../img/seatWhite.svg'
import chairGreen from '../../../img/seatGreen.svg'

const Seat = ({ style, id }) => {
	const { deleteSeat, addSeat } = useTicket()
	const [isActiveSeat, setIsActiveSeat] = useState(false)

	const toggleActiveBtn = e => {
		isActiveSeat ? deleteSeat(e.target.parentElement.id, e.target.id) : addSeat(e.target.parentElement.id, e.target.id)
		isActiveSeat ? (e.target.src = chairWhite) : (e.target.src = chairGreen)
		setIsActiveSeat(prev => !prev)
	}

	return (
		<>
			<img
				onClick={e => {
					toggleActiveBtn(e)
				}}
				id={id}
				src={chairWhite}
				alt="chair"
				className={style}
			/>
		</>
	)
}

export default Seat
