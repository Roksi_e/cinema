import { useEffect, useMemo, useState } from 'react'
import { v4 as uuidv4 } from 'uuid'
import Button from '../../button/button'
import { useTicket } from '../../../methods/tickets'
import date from '../../../img/date.svg'
import ticket from '../../../img/ticket.svg'
import cost from '../../../img/cost.svg'

import styles from './ticketInfo.module.css'

const TicketInfo = () => {
	const { tickets, seats, openForm } = useTicket()
	const [chooseTicket, setChooseTicket] = useState([])
	// eslint-disable-next-line no-unused-vars
	const [buyTickets, setBuyTickets] = useState(
		localStorage.getItem('clientTickets') ? JSON.parse(localStorage.getItem('clientTickets')) : []
	)

	useEffect(() => {
		clientTickets(tickets, seats)
	}, [seats, tickets])

	function clientTickets(obj, arr) {
		let ticketOne = arr.map((item, index) => {
			let ticket = {
				id: uuidv4(),
				name: obj.name,
				image: obj.image,
				date: obj.date,
				session: obj.session,
				price: obj.price.price,
				hall: obj.hall,
				row: item.row,
				seat: item.seat
			}
			return ticket
		})
		setChooseTicket(ticketOne)
	}

	const clientTicketsSave = chooseTicket => {
		setBuyTickets(chooseTicket)
		localStorage.setItem('clientTickets', JSON.stringify(chooseTicket))
		openForm()
	}

	const totalPrice = useMemo(() => {
		let totalPrice = seats.reduce((acc, el) => {
			return acc + tickets.price.price
		}, 0)

		return totalPrice
	}, [seats, tickets.price.price])

	return (
		<div className={styles.cinemaBottom}>
			<div className={styles.ticketInfo}>
				<div className={styles.ticketInfoBlock}>
					<div className={styles.ticketRow}>
						<img src={date} alt="date" />
						<div className={styles.ticketInfoFilm}>{tickets.date}</div>
					</div>
					<div className={styles.ticketRow}>
						<div className={styles.ticketCircle}></div>
						<div className={styles.ticketInfoTitle}>{tickets.session}</div>
					</div>
				</div>
				<div className={styles.ticketInfoBlock}>
					<div className={styles.ticketRow}>
						<img src={ticket} alt="ticket" />
						<div className={styles.ticketInfoFilm}>{tickets.name}</div>
					</div>
					<div className={styles.ticketRow}>
						<div className={styles.ticketCircle}></div>
						<ul className={styles.ticketSeats}>
							{seats
								? seats.map((el, index) => {
										return (
											<li key={index}>
												row: {el.row} seat: {el.seat}
											</li>
										)
								  })
								: null}
						</ul>
					</div>
				</div>
				{seats.length ? (
					<div className={styles.ticketInfoBlock}>
						<img src={cost} alt="cost" />
						<div className={styles.ticketInfoTitle}>{`Cost ${totalPrice} UAH`}</div>
					</div>
				) : null}
			</div>
			<div className={styles.bottomBtn}>
				<Button
					disabled={!chooseTicket.length ? true : false}
					className={styles.btn}
					onClick={() => clientTicketsSave(chooseTicket)}
				/>
			</div>
		</div>
	)
}

export default TicketInfo
