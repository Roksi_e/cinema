import { useEffect, useRef } from 'react'
import chairRed from '../../../img/seatRed.svg'
import Seat from '../seat/seat'

import styles from './cinemaSeats.module.css'

const CinemaSeats = () => {
	const seatsCol = new Array(14)
	const seatsRow = new Array(10)

	const elRef = useRef()

	useEffect(() => {
		unavailable()
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])

	function randomItem(array) {
		let n = Math.floor(Math.random() * (14 / 3))
		for (let i = 0; i < n; i++) {
			let r = Math.floor(Math.random() * (array.length - i) + i)
			array[r].src = chairRed
		}
	}

	// eslint-disable-next-line react-hooks/exhaustive-deps
	function unavailable() {
		const [...seats] = elRef.current.children
		seats.forEach(el => {
			const [...all] = el.children
			randomItem(all)
		})
	}

	return (
		<div className={styles.seatsWrap} ref={elRef}>
			{seatsRow.fill().map((_, i) => {
				return (
					<div key={'chairRow' + i * 8} className={styles.rows} id={i + 1}>
						{seatsCol.fill().map((_, i) => {
							return <Seat style={styles.seats} id={i + 1} key={'chairCol' + i + 1} />
						})}
					</div>
				)
			})}
		</div>
	)
}

export default CinemaSeats
