import styles from "./seatsBottom.module.css";

const SeatsBottom = () => {
  return (
    <div className={styles.seatsBottom}>
      <div className={styles.seatsChoose}>
        <div
          style={{ backgroundColor: "white" }}
          className={styles.seatsChooseCircle}
        ></div>
        <div className={styles.seatsChooseText}>Available</div>
      </div>
      <div className={styles.seatsChoose}>
        <div
          style={{ backgroundColor: "red" }}
          className={styles.seatsChooseCircle}
        ></div>
        <div className={styles.seatsChooseText}>Unavailable</div>
      </div>
      <div className={styles.seatsChoose}>
        <div
          style={{ backgroundColor: "rgb(56 233 175)" }}
          className={styles.seatsChooseCircle}
        ></div>
        <div className={styles.seatsChooseText}>Selected</div>
      </div>
    </div>
  );
};

export default SeatsBottom;
