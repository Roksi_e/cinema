import React, { useState } from 'react'
import { Calendar } from 'react-multi-date-picker'
import { useForm, Controller } from 'react-hook-form'
import { useTicket } from '../../methods/tickets'

export default function CinemaCalendar({ className }) {
	const { control, handleSubmit } = useForm()
	const { closeCalendar } = useTicket()
	// eslint-disable-next-line no-unused-vars
	const [submittedDate, setSubmittedDate] = useState()

	const minDate = new Date()
	const maxDate = new Date(minDate.getFullYear(), minDate.getMonth() + 1, minDate.getDate())

	const onSubmit = ({ date }) => {
		setSubmittedDate(date)
		closeCalendar(date.format?.('MMMM D YYYY'))
	}

	return (
		<>
			<form onSubmit={handleSubmit(onSubmit)}>
				<Controller
					control={control}
					name="date"
					rules={{ required: true }} //optional
					render={({ field: { onChange, name, value }, fieldState: { invalid, isDirty }, formState: { errors } }) => (
						<>
							<Calendar
								value={value || ''}
								onChange={date => {
									onChange(date?.isValid ? date : '')
								}}
								minDate={minDate}
								maxDate={maxDate}
								format={'MM/DD/YYYY'}
							/>
							{errors && errors[name] && errors[name].type === 'required' && <span>your error message !</span>}
						</>
					)}
				/>
				<input type="submit" value="Confirm date" className={className} />
			</form>
		</>
	)
}
