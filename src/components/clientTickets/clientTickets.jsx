import { useEffect, useState } from 'react'
import { AiOutlineClose } from 'react-icons/ai'
import { useTicket } from '../../methods/tickets'
import clientCode from '../../img/barcode (1).gif'

import styles from './clientTickets.module.css'

const ClientTickets = () => {
	const { closeTikets } = useTicket()
	const [clientTickets, setClientTickets] = useState(
		localStorage.getItem('clientTickets') ? JSON.parse(localStorage.getItem('clientTickets')) : []
	)

	useEffect(() => {
		getClientTickets()
	}, [])

	function getClientTickets() {
		const tickets = JSON.parse(localStorage.getItem('clientTickets'))
		setClientTickets(tickets)
	}

	return (
		<div className={styles.clientTicketsWrap}>
			<AiOutlineClose onClick={() => closeTikets()} className={styles.close} />
			<h2>Thank you for your choice. Enjoy watching!</h2>
			<div className={styles.box}>
				{clientTickets.map((item, index) => {
					return (
						<div key={index} className={styles.card}>
							<div className={styles.infoBlock}>
								<div className={styles.image}>
									<img src={item.image} alt="banner" />
								</div>
								<div className={styles.ticketInfo}>
									<h4 className={styles.ticketTitle}>{item.name}</h4>
									<div className={styles.infoItems}>
										<span>Date: {item.date}</span>
										<span>Hour: {item.session}</span>
										<span>Hall: {item.hall}</span>
									</div>

									<div className={styles.ticketSeat}>
										<div className={styles.seats}>Row: {item.row}</div>
										<div>Seat: {item.seat}</div>
									</div>
								</div>
							</div>

							<div className={styles.imageCode}>
								<img src={clientCode} alt="code" />
							</div>
						</div>
					)
				})}
			</div>
		</div>
	)
}

export default ClientTickets
