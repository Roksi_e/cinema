import { useContext, useState } from 'react'
import { Link } from 'react-router-dom'
import { LuSearch } from 'react-icons/lu'
import { MyContext } from '../../methods/context'

import styles from './header.module.css'

const Header = () => {
	const [searchText, setSearchText] = useState('')
	const [searchData, setSearchData] = useState(null)
	const [isOpen, setIsOpen] = useState(true)
	const [linkId, setLinkId] = useState(null)
	const allData = useContext(MyContext)

	function filter(value) {
		if (!value) return
		if (allData.films !== null) {
			const newArr = [...allData.films].filter(el => {
				return el.name.toLowerCase().includes(value.toLowerCase())
			})
			let id = newArr.map(e => {
				return e.id
			})
			setLinkId(id)
			return setSearchData(newArr)
		}
	}

	const search = value => {
		setSearchText(value)
		filter(searchText)
	}

	const iconClick = () => {
		filter(searchText)
		setSearchText('')
		setSearchData(null)
	}

	const click = e => {
		setIsOpen(!isOpen)
		setSearchText(e.target.textContent)
		filter(e.target.textContent)
	}

	const inputClick = () => {
		setIsOpen(true)
		setSearchData(null)
	}

	return (
		<div className={styles.headerWrap}>
			<div className={styles.header}>
				<Link to="/" relative="route" className={styles.logoLink}>
					<div className={styles.headerTitle}>Vilm</div>
				</Link>

				<div className={styles.headerSearch}>
					<input
						type="search"
						value={searchText}
						onChange={e => {
							search(e.target.value)
						}}
						onClick={inputClick}
					/>
					<ul className={styles.searchList}>
						{searchData && isOpen
							? searchData.map((e, i) => {
									return (
										<li onClick={click} key={i} className={styles.searchItems}>
											{e.name}
										</li>
									)
							  })
							: null}
					</ul>
					{linkId ? (
						<Link
							className={styles.link}
							to={`films/${linkId[0]}`}
							onClick={e => {
								iconClick()
							}}
						/>
					) : null}
					<LuSearch className={styles.headerIcon} />
				</div>
			</div>
		</div>
	)
}

export default Header
