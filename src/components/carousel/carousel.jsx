import { useContext, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { v4 as uuidv4 } from 'uuid'
import { BsStarFill } from 'react-icons/bs'
import Carousel from 'react-bootstrap/Carousel'
import { MyContext } from '../../methods/context'

import 'bootstrap/dist/css/bootstrap.min.css'
import './carousel.css'

function CarouselMain() {
	const [randomFilm, setRandomFilm] = useState([])
	const allData = useContext(MyContext)

	function randomItem(array) {
		let arr = []
		for (let i = 0; i < 4; i++) {
			let r = Math.floor(Math.random() * (array.length - i) + i)
			arr = [...new Set(arr), array[r]]
		}
		return setRandomFilm(arr)
	}

	useEffect(() => {
		if (allData !== null && allData.films !== null) {
			randomItem(allData.films)
		}
	}, [allData, allData.films])

	return (
		<Carousel fade>
			{randomFilm
				? randomFilm.map(el => {
						return (
							<Carousel.Item key={uuidv4()}>
								<img className="d-block w-100" src={el.backdrop} alt="backdrop" />
								<Carousel.Caption>
									<Link to={`films/${el.id}`} className="slider_link">
										<div className="div_title">
											<h3 className="carousel_title">{el.name}</h3>
										</div>
									</Link>
									<div className="div_title">
										<p>{el.overview}</p>
									</div>
									<div className="carousel_bottom">
										<div className="div_rate">
											<BsStarFill className="carousel_icon" />
											<div>{el.rate.toFixed(1)}</div>
										</div>
									</div>
								</Carousel.Caption>
							</Carousel.Item>
						)
				  })
				: null}
		</Carousel>
	)
}

export default CarouselMain
