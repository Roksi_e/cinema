const Button = ({ className, onClick, disabled }) => {
	return (
		<>
			<button
				onClick={() => {
					onClick()
				}}
				className={className}
				disabled={disabled}
			>
				Buy tickets
			</button>
		</>
	)
}

export default Button
