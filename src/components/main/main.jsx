import { useState } from 'react'
import Offcanvas from 'react-bootstrap/Offcanvas'
import CarouselMain from '../carousel/carousel'
import Cards from '../cards/cards'
import Stars from './stars/stars'
import { Footer } from '../footer/footer'

import styles from './main.module.css'

const Main = () => {
	const [openStar, setOpenStar] = useState(null)
	const [show, setShow] = useState(false)

	const handleClose = () => setShow(false)

	const handleShow = id => {
		setShow(true)
		setOpenStar(id)
	}

	return (
		<div className={styles.mainWrap}>
			<CarouselMain />
			<div className={styles.movieContainer}>
				<Cards click={handleShow} />
			</div>
			<Offcanvas
				show={show}
				onHide={handleClose}
				placement={'bottom'}
				style={{ height: '80vh', backgroundColor: 'black' }}
			>
				<Offcanvas.Header closeButton closeVariant="white">
					<Offcanvas.Title style={{ color: 'white' }}>More information about stars</Offcanvas.Title>
				</Offcanvas.Header>
				<Offcanvas.Body>
					<Stars id={openStar} />
				</Offcanvas.Body>
			</Offcanvas>
			<Footer />
		</div>
	)
}

export default Main
