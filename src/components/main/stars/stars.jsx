import { useContext } from 'react'
import { v4 as uuidv4 } from 'uuid'
import { MyContext } from '../../../methods/context'
import Card from '../../cards/card/card'

import styles from './stars.module.css'

const Stars = ({ id }) => {
	const allData = useContext(MyContext)

	const star = allData.stars.find(star => {
		return star.id === id
	})

	return (
		<>
			<div className={styles.starWrap}>
				<div className={styles.starImage}>
					<img src={star.image} alt={star.name} />
					<h3>{star.name}</h3>
				</div>
				<div className={styles.starInfoBlock}>
					<h3>Can you see in:</h3>
					<div className={styles.films}>
						{star.films.map(el => {
							if (el.name === undefined) return null

							return (
								<Card key={uuidv4()}>
									<div className={styles.itemImg}>
										<img src={el.image} alt={el.name} />
									</div>

									<div className={styles.itemBottom}>
										<div className={styles.itemTitle}>{el.name}</div>
									</div>
								</Card>
							)
						})}
					</div>
				</div>
			</div>
		</>
	)
}

export default Stars
