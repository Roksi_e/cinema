import { useState } from 'react'
import { AiOutlineClose } from 'react-icons/ai'
import { v4 as uuidv4 } from 'uuid'
import { useForm } from 'react-hook-form'
import MaskedInput from 'react-input-mask'
import { useTicket } from '../../methods/tickets'

import styles from './controlForm.module.css'

export default function ControlForm() {
	const { buyTikets, seats, tickets, closeForm } = useTicket()
	// eslint-disable-next-line no-unused-vars
	const [clientInfo, setClientInfo] = useState(
		localStorage.getItem('clientInfo') ? JSON.parse(localStorage.getItem('clientInfo')) : []
	)

	const {
		register,
		handleSubmit,
		reset,
		formState: { errors }
	} = useForm({ mode: 'onBlur' })

	const onSubmit = data => {
		buyTikets()
		setClientInfo(data)
		localStorage.setItem('clientInfo', JSON.stringify(data))
		reset({ firstName: '', email: '', phone: '', lastName: '' })
	}

	return (
		<section>
			<div className={styles.formWrap}>
				<AiOutlineClose
					onClick={() => {
						closeForm()
					}}
					className={styles.cinemaClose}
				/>
				<div className={styles.clientInfo}>
					<h3>Check your order</h3>
					<p>
						<span className={styles.ticketInfo}>Movie: </span>
						{tickets.name}
					</p>
					<p>
						<span className={styles.ticketInfo}>Date: </span>
						{tickets.date}
					</p>
					<p>
						<span className={styles.ticketInfo}>Hour: </span>
						{tickets.session}
					</p>
					{seats.map(el => {
						return (
							<div key={uuidv4()} className={styles.seats}>
								<span className={styles.ticketInfo}>Row: </span>
								<span>{el.row}</span>
								<span className={styles.ticketInfo}>Seat: </span>
								<span>{el.seat}</span>
							</div>
						)
					})}
				</div>

				<div className={styles.formBlock}>
					<p>Fill out the form. We will send your tickets by email</p>
					<form onSubmit={handleSubmit(onSubmit)}>
						<div className={styles.contactForm}>
							<label htmlFor="name">Name:</label>
							<input
								{...register('firstName', {
									required: `This field is required`,
									pattern: { value: /^[A-Za-z]+$/i, message: 'Enter correct name' },
									minLength: {
										value: 2,
										message: 'Name must have at least 2 characrers'
									}
								})}
								placeholder="Enter your first name"
								style={{
									border: errors.firstName ? '1px solid red' : '1px solid gray'
								}}
							/>
							{errors?.firstName?.message && <span className={styles.error}>{errors.firstName.message}</span>}
						</div>

						<div className={styles.contactForm}>
							<label htmlFor="lastName">Last name:</label>
							<input
								{...register('lastName', {
									required: `This field is required`,
									pattern: { value: /^[A-Za-z]+$/i, message: 'Enter correct data' },
									minLength: {
										value: 2,
										message: 'Name must have at least 2 characrers'
									}
								})}
								placeholder="Enter your last name"
								style={{
									border: errors.lastName ? '1px solid red' : '1px solid gray'
								}}
							/>
							{errors?.lastName?.message && <span className={styles.error}>{errors.lastName.message}</span>}
						</div>

						<div className={styles.contactForm}>
							<label htmlFor="phone">Phone:</label>
							<MaskedInput
								{...register('phone', {
									required: `This field is required`,
									pattern: { value: /^\+380\d{3}\d{2}\d{2}\d{2}$/, message: 'Enter correct phone number' },
									minLength: {
										value: 5,
										message: 'Must be a valid phone number'
									}
								})}
								mask="+380999999999"
								placeholder="+380"
								style={{
									border: errors.phone ? '1px solid red' : '1px solid gray'
								}}
							/>
							{errors?.phone?.message && <span className={styles.error}>{errors.phone.message}</span>}
						</div>

						<div className={styles.contactForm}>
							<label htmlFor="email">Email:</label>
							<input
								type="email"
								{...register('email', {
									required: `This field is required`,
									pattern: { value: /\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/gi, message: 'Enter correct email' },
									minLength: {
										value: 4,
										message: 'Must be a valid email'
									}
								})}
								placeholder="Enter email"
								style={{
									border: errors.email ? '1px solid red' : '1px solid gray'
								}}
							/>
							{errors?.email?.message && <span className={styles.error}>{errors.email.message}</span>}
						</div>

						<div className={styles.contactBtn}>
							<input type="submit" value="Confirm your order" className={styles.btnSubmit} />
						</div>
					</form>
				</div>
			</div>
		</section>
	)
}
