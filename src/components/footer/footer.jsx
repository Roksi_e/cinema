import styles from "./footer.module.css";

export const Footer = () => {
  const date = new Date().getFullYear();
  return (
    <div className={styles.footer}>
      <div className={styles.footerContent}>
        <div className={styles.footerTitle}>
          <span>Vilm</span> © 2021-{date}
        </div>
        <div className={styles.footerName}>powered by tmdb</div>
      </div>
    </div>
  );
};
