import {
	ADD_SEAT,
	ADD_SESSION,
	BUY_TICKETS,
	CLOSE_CALENDAR,
	CLOSE_FORM,
	CLOSE_HALL,
	CLOSE_TICKETS,
	DELETE_SEAT,
	DELETE_SESSION,
	OPEN_CALENDAR,
	OPEN_FORM,
	OPEN_HALL
} from './types'
import { addPrice, months } from '.'

const handlers = {
	[ADD_SESSION]: (state, { value, obj }) => {
		return {
			...state,
			tickets: {
				name: obj.name,
				id: obj.id,
				price: addPrice(value),
				session: value,
				image: obj.image,
				hall: 'Blue sky',
				date: `${months[new Date().getMonth()]} ${new Date().getDate()} ${new Date().getFullYear()}`
			}
		}
	},
	[DELETE_SESSION]: (state, { value, obj }) => {
		return {
			...state,
			tickets: { name: '', id: '', session: '' }
		}
	},
	[OPEN_HALL]: state => {
		return {
			...state,
			isOpen: true
		}
	},
	[CLOSE_HALL]: state => {
		return {
			...state,
			tickets: {},
			isOpen: false
		}
	},
	[ADD_SEAT]: (state, { r, s }) => {
		return {
			...state,
			seats: [...state.seats, { row: r, seat: s }]
		}
	},
	[DELETE_SEAT]: (state, { r, s }) => {
		return {
			...state,
			// eslint-disable-next-line array-callback-return
			seats: state.seats.filter((item, index) => {
				return (item.row && item.seat) !== (r && s)
			})
		}
	},
	[BUY_TICKETS]: state => {
		return {
			...state,
			isActive: true,
			isOpenForm: false
		}
	},
	[CLOSE_TICKETS]: state => {
		return {
			...state,
			tickets: {},
			seats: [],
			isActive: false
		}
	},
	[OPEN_CALENDAR]: state => {
		return {
			...state,
			isOpenCalendar: true
		}
	},
	[CLOSE_CALENDAR]: (state, { value }) => {
		return {
			...state,
			tickets: { ...state.tickets, date: value },
			isOpenCalendar: false
		}
	},
	[OPEN_FORM]: state => {
		return {
			...state,
			isOpenForm: true,
			isOpen: false
		}
	},
	[CLOSE_FORM]: state => {
		return {
			...state,
			seats: [],
			isOpenForm: false,
			isOpen: true
		}
	},
	DEFAULT: state => state
}

export const ticketReducer = (state, action) => {
	const handler = handlers[action.type] || handlers.DEFAULT
	return handler(state, action)
}
