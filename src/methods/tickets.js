import { createContext, useContext, useReducer } from 'react'
import { ticketReducer } from './reducer'
import {
	ADD_SEAT,
	ADD_SESSION,
	BUY_TICKETS,
	CLOSE_CALENDAR,
	CLOSE_FORM,
	CLOSE_HALL,
	CLOSE_TICKETS,
	DELETE_SEAT,
	DELETE_SESSION,
	OPEN_CALENDAR,
	OPEN_FORM,
	OPEN_HALL
} from './types'

const TicketContext = createContext()

export const useTicket = () => {
	return useContext(TicketContext)
}

export const TicketProvider = ({ children }) => {
	const initialState = {
		tickets: {},
		seats: [],
		totalPrice: '',
		isOpen: false,
		isOpenCalendar: false,
		isActive: false,
		isOpenForm: false
	}

	const [state, dispatch] = useReducer(ticketReducer, initialState)

	const addSession = (value, obj) => dispatch({ type: ADD_SESSION, value, obj })
	const deleteSession = (value, obj) => dispatch({ type: DELETE_SESSION, value, obj })
	const openHall = () => dispatch({ type: OPEN_HALL })
	const closeHall = () => dispatch({ type: CLOSE_HALL })
	const addSeat = (r, s) => dispatch({ type: ADD_SEAT, r, s })
	const deleteSeat = (r, s) => dispatch({ type: DELETE_SEAT, r, s })
	const buyTikets = () => dispatch({ type: BUY_TICKETS })
	const closeTikets = () => dispatch({ type: CLOSE_TICKETS })
	const openCalendar = () => dispatch({ type: OPEN_CALENDAR })
	const closeCalendar = value => dispatch({ type: CLOSE_CALENDAR, value })
	const openForm = () => dispatch({ type: OPEN_FORM })
	const closeForm = () => dispatch({ type: CLOSE_FORM })

	return (
		<TicketContext.Provider
			value={{
				...state,
				addSession,
				deleteSession,
				openHall,
				closeHall,
				addSeat,
				deleteSeat,
				buyTikets,
				closeTikets,
				openCalendar,
				closeCalendar,
				openForm,
				closeForm
			}}
		>
			{children}
		</TicketContext.Provider>
	)
}
