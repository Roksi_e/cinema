import empty from '../img/photo-empty.png'

export async function req(url, b) {
	const rez = await fetch(url)
	return await rez.json()
}

export function createValidPerson(oldArray) {
	if (Array.isArray(oldArray)) return null
	let [...arr] = oldArray.results
	arr = arr.map(obj => {
		return {
			name: obj.name,
			id: obj.id,
			films: obj.known_for.map(film => {
				return createObj(film)
			}),
			image: obj.profile_path ? `https://image.tmdb.org/t/p/w500${obj.profile_path}` : empty
		}
	})
	return arr
}

export function createvalidMovie(oldArray) {
	if (Array.isArray(oldArray)) return null
	let [...arr] = oldArray.results
	arr = arr.map(obj => {
		return createObj(obj)
	})
	return arr
}

function createObj(el) {
	return {
		name: el.title,
		id: el.id,
		release_date: el.release_date,
		image: `https://image.tmdb.org/t/p/w500${el.poster_path}`,
		backdrop: `https://image.tmdb.org/t/p/original${el.backdrop_path}`,
		overview: el.overview,
		rate: el.vote_average,
		price: [
			{ point: 'night', price: 60 },
			{ point: 'day', price: 90 },
			{ point: 'morning', price: 70 },
			{ point: 'evening', price: 120 },
			{ point: 'premiere', price: 170 }
		],
		session: [{ time: '9:00' }, { time: '12:15' }, { time: '17:45' }, { time: '21:00' }]
	}
}

export function errorToLS(err) {
	const newError = {
		status: err.status,
		statusText: err.statusText
	}
	return localStorage.setItem('responseError', JSON.stringify(newError))
}

export function addPrice(value) {
	if (value === '9:00') {
		return { point: 'morning', price: 70 }
	}
	if (value === '12:15') {
		return { point: 'day', price: 90 }
	}
	if (value === '17:45') {
		return { point: 'evening', price: 120 }
	}
	if (value === '21:00') {
		return { point: 'night', price: 60 }
	}
}

export const months = [
	'January',
	'February',
	'March',
	'April',
	'May',
	'June',
	'July',
	'August',
	'September',
	'October',
	'November',
	'December'
]
