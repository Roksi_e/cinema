import { Route, Routes } from 'react-router-dom'
import { useEffect, useState } from 'react'
import Main from './components/main/main'
import { createValidPerson, createvalidMovie, errorToLS } from './methods'
import { MyContext } from './methods/context'
import Header from './components/header/header'
import Details from './components/details/details'
import { TicketProvider } from './methods/tickets'

import './App.css'

function App() {
	const [reqDataPerson, setReqDataPerson] = useState([])
	const [reqDataMovie, setReqDataMovie] = useState([])
	const [error, setError] = useState({})
	const [isLoading, setisLoading] = useState(true)
	const [stars, setStars] = useState([])
	const [films, setFilms] = useState([])

	try {
		useEffect(() => {
			const options = {
				method: 'GET',
				headers: {
					accept: 'application/json',
					Authorization:
						'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI3NTYzNzc0MDk5MTkwZTdlZTEyMmRlZTM1MzVlOGU3NSIsInN1YiI6IjY0YjI5ODVkMjNkMjc4MDBjOTNiYTI0MyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.Jbemps_MCpCT-LruA9-XfhO2sKJCdCwTF2q_7M7lSgU'
				}
			}

			fetch('https://api.themoviedb.org/3/person/popular', options)
				.then(response => {
					if (response.status >= 200 && response.status < 300) {
						return response.json()
					} else {
						errorToLS(response)
					}
				})
				.then(response => setReqDataPerson(response))
				.catch(err =>
					setError({
						message: err.message,
						name: err.name,
						stack: err.stack
					})
				)
				.finally(() => setisLoading(false))

			fetch('https://api.themoviedb.org/3/discover/movie', options)
				.then(response => {
					if (response.status >= 200 && response.status < 300) {
						return response.json()
					} else {
						console.log(response)
						errorToLS(response)
					}
				})
				.then(response => setReqDataMovie(response))
				.catch(err =>
					setError({
						message: err.message,
						name: err.name,
						stack: err.stack
					})
				)
				.finally(() => setisLoading(false))
		}, [])

		useEffect(() => {
			if (reqDataPerson !== null && reqDataPerson !== undefined) {
				const people = createValidPerson(reqDataPerson)
				setStars(people)
			}
			if (reqDataMovie !== null && reqDataMovie !== undefined) {
				const movie = createvalidMovie(reqDataMovie)
				setFilms(movie)
			}
			localStorage.setItem('new-error', JSON.stringify(error))
		}, [reqDataPerson, reqDataMovie, error])
	} catch (err) {
		setError({
			message: err.message,
			name: err.name,
			stack: err.stack
		})
		localStorage.setItem('new-error', JSON.stringify(error))

		throw new Error(err)
	}

	const allData = { stars: stars, films: films }
	if (isLoading) {
		return <h1>Loading...</h1>
	}

	return (
		<div className="App">
			<MyContext.Provider value={allData}>
				<TicketProvider>
					<Header />
					<Routes>
						<Route index element={<Main />} />
						<Route path="films/:id" element={<Details />} />
					</Routes>
				</TicketProvider>
			</MyContext.Provider>
		</div>
	)
}

export default App
